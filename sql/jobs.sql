drop table if exists jobs;

create table jobs (
    id integer not null primary key,
    jobTitle varchar(80) not null,
    company varchar(80) not null,
    salary integer not null,
    location varchar(40) not null,
    description text default ''
);

insert into jobs values (null, "I.T. Support",  "Supporticon", 10,000, "Gold Coast", "Need someone capable of using C++, Java, 3DS max and k2 Enginge");
