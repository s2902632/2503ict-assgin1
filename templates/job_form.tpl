{extends file='home.tpl'}

{block name=title}
Job Seach - Search Job
{/block}

{block name=body}
<div class="col-sm-9">
              <form role="form" method="post" action="add_job_action.php">
                  <div class="form-group">
                    <label>Job Title</label>
                    <input type="text" class="form-control" id="jobTitle" placeholder="Enter Job Title">
                  </div>
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control" id="company" placeholder="Enter Company">
                  </div>
                  <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" id="salary" placeholder="Enter Salary">
                  </div>
                  <div class="form-group">
                    <label>Location</label>
                    <input type="text" class="form-control" id="location" placeholder="Enter Location">
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" id="description" placeholder="Enter Description">
                  </div>
                  <button type="submit" class="btn btn-default">Submit</button>
                </form>
</div>

{/block}