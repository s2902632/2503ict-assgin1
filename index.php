<?php

require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

date_default_timezone_set('UTC');

if (isset($_GET['query'])) {
    $query = $_GET['query'];
} else {
    $query = "";
}

$items = get_items($query);
// print_r($items);
$smarty = new Smarty;

$smarty->assign("query",$query);
$smarty->assign("items",$items);

$smarty->display("home.tpl");
?>
