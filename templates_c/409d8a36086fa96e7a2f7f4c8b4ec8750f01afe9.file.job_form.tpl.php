<?php /* Smarty version Smarty-3.1.16, created on 2014-04-28 10:25:07
         compiled from "./templates/job_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1177813670535785771da5c0-41273271%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '409d8a36086fa96e7a2f7f4c8b4ec8750f01afe9' => 
    array (
      0 => './templates/job_form.tpl',
      1 => 1398679114,
      2 => 'file',
    ),
    '62ef71fa9bffee4b2e45ea97bf20c2caac4cf263' => 
    array (
      0 => './templates/home.tpl',
      1 => 1398244458,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1177813670535785771da5c0-41273271',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53578577243b24_27699756',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53578577243b24_27699756')) {function content_53578577243b24_27699756($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
Job Seach - Search Job
</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Search</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#index.html">Home</a></li>
            <li><a href="#about.html">About</a></li>
            <li><a href="#contact.html">Contact</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="#sign.html">Sign In</a></li>
          </ul>
        </div>
      </div>
    </div>
    
    <div id="main-body" class="container">
        <div class="row">
            <div class="col-sm-3">
              <div class="list-group">
                <a class="list-group-item" href="index.php">Home</a>
                <a class="list-group-item" href="page2.html">List all jobs</a>
                <a class="list-group-item" href="page3.html">Search for a job</a>
                <a class="list-group-item" href="page4.html">List of Employers</a>
                <a class="list-group-item" href="job_form.php">Advertise a Job</a>
              </div>
            </div>
            <div class="col-sm-9">
              
<div class="col-sm-9">
              <form role="form" method="post" action="add_job_action.php">
                  <div class="form-group">
                    <label>Job Title</label>
                    <input type="text" class="form-control" id="jobTitle" placeholder="Enter Job Title">
                  </div>
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control" id="company" placeholder="Enter Company">
                  </div>
                  <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" id="salary" placeholder="Enter Salary">
                  </div>
                  <div class="form-group">
                    <label>Location</label>
                    <input type="text" class="form-control" id="location" placeholder="Enter Location">
                  </div>
                  <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" id="description" placeholder="Enter Description">
                  </div>
                  <button type="submit" class="btn btn-default">Submit</button>
                </form>
</div>


            </div>
        </div>
    </div>

  </body>
</html><?php }} ?>
