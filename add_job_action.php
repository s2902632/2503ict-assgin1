<?php
/*
 * Adds new item from form data.
 */
require '../Smarty/libs/Smarty.class.php';
require "includes/defs.php";

# Get form data
$jobTitle = $_POST['jobTitle'];
$company = $_POST['company'];
$salary = $_POST['salary'];
$location = $_POST['location'];
$description = $_POST['description'];

# Check data is valid
if (empty($jobTitle) || empty($company) || empty($salary) || empty($location) || empty($description)) {
    $error = "Field must not be empty.";
    header("Location: add_job.php?error=$error");
    exit;
}

# add new item with form data
$id = add_job($jobTitle,$company,$salary,$location,$description);

header("Location: job_detail.php?id=$id"); 
exit;
?>
